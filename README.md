# Setup the project

## Step 1: Generating oauthMailToken.json
1. Make sure to install node and open the terminal
2. Run ```npm install ```
3. Run ```mkdir .gcp``` and place the downloaded GCP Oauth Json file inside .gcp folder. Rename it as "oauthMailKey.json"
4. Then run ```npm run genOauthToken``` to generate "oauthMailToken.json" in .gcp folder.

## Step 2: Install Redis commander
1. If havent install redis-commander, run ```npm install -g redis-commander```
2. Run ```redis-commander```
3. Open http://127.0.0.1:8081, then we could see our redis data and queue. 

## Step 3: Run the application
1. Run ```npm run api``` for the api (queue producer)
2. Run ```npm run worker``` for the worker (queue consumer)
3. Check the api sample in examples/testApi.json, run ```npm run api:test``` to execute.
4. Please customize the examples/mailTemplate.ts, modify "sender" params to be the email of the respective GCP authorized user who granted the permission to send the email.
5. Please pass in "x-channel-id" and "x-api-key" in the request headers every time you send the mail request.