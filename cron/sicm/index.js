const cron = require('node-cron')
const axios = require('axios')
const moment = require('moment')

async function SendSummary() {
  console.log(`Start time: ${moment().format('YYYY-MM-DD HH:mm:ss')}`)
  console.log('Sending report submission summary mail')
  const yearMonth = moment()
    .subtract(1, 'months')
    .endOf('month')
    .format('YYYYMM')

  await axios.post(
    `http://localhost:${process.env.PORT}/api/sicm/report/summary`,
    {
      yearMonth
    },
    {
      timeout: 0,
      headers: {
        'x-api-key': process.env.API_KEY,
        'x-channel-id': process.env.CHANNEL_ID
      }
    }
  )
  console.log('Ok')
  console.log(`End time: ${moment().format('YYYY-MM-DD HH:mm:ss')}`)
}

async function SendReminder() {
  console.log(`Start time: ${moment().format('YYYY-MM-DD HH:mm:ss')}`)

  console.log('Sending submit report reminder mail')

  const yearMonth = moment()
    .subtract(1, 'months')
    .endOf('month')
    .format('YYYYMM')

  await axios.post(
    `http://localhost:${process.env.PORT}/api/sicm/report/reminder`,
    {
      yearMonth
    },
    {
      timeout: 0,
      headers: {
        'x-api-key': process.env.API_KEY,
        'x-channel-id': process.env.CHANNEL_ID
      }
    }
  )
  console.log('Ok')
  console.log(`End time: ${moment().format('YYYY-MM-DD HH:mm:ss')}`)
}

module.exports = function SICMCron() {
  // start dry run after 10 sec
  // if (process.env.STAGE !== 'production' || process.env.DRY_RUN === 'enabled')
  //   setTimeout(() => {
  //     console.log('Start dry run')
  //     SendSummary().catch(console.error)
  //     SendReminder().catch(console.error)
  //     console.log('Complete dry run')
  //   }, 3 * 1000)

  cron.schedule('0 0 9,12,15 * *', () =>
    SendSummary().catch(console.error)
  )

  cron.schedule('0 0 9,12,15 * *', () =>
    SendReminder().catch(console.error)
  )
  console.log(
    `[${process.env.STAGE}${
      process.env.DRY_RUN === 'enabled' ? ' DRY RUN' : ''
    }] Cron Job running on background.`
  )
}
