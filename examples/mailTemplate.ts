import { IMailPayload } from 'shared/types/mailer'
import mjml2html from 'mjml'
import { compile } from 'handlebars'
import * as fs from 'fs'
import { resolve } from 'path'

let WelcomeTemplate = mjml2html(
  fs.readFileSync('./examples/mjml/Welcome.mjml', 'utf8'),
  {
    beautify: true,
    filePath: resolve('./examples/mjml'),
    minify: true
  }
)

const template: IMailPayload = {
  receipents: [{ name: 'CHAI CHEAH WEN', email: 'cheahwen1997@gmail.com' }],
  subject: `[Sample] Welcome`,
  html: compile(WelcomeTemplate?.html)({ system_email_name: 'CW' }),
  attachments: [
    './data/invoice.pdf',
    './data/invoice3.pdf',
    './data/photo.jpeg'
  ]
}

export default template
