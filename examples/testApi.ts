import axios from 'axios'
import template from './mailTemplate'

async function sendMail() {
  await axios.post(
    `http://localhost:4000/api/mail`,
    { ...template, subject: template.subject },
    {
      headers: {
        'x-channel-id': 'sicm',
        'x-api-key': '590a40acb162414937c436412cdf1028'
      }
    }
  )
}

console.log('Sending mail')
sendMail()
  .then(() => {
    console.log('Success sent mail')
  })
  .catch((e) => console.error(e))
