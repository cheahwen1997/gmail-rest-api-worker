import { NextFunction, Request, Response } from 'express'

export default (req: Request, res: Response, next: NextFunction) => {
  const apiKey = req.headers['x-api-key']
  const channelId = req.headers['x-channel-id']
  if (!apiKey) throw new Error('No api key provided')
  if (!channelId) throw new Error(`x-channel-id is not specified`)

  if (channelId !== process.env.CHANNEL_ID)
    throw new Error(`x-channel-id is invalid`)

  if (apiKey !== process.env.API_KEY) throw new Error(`x-api-key is invalid`)

  next()
}
