import { NextFunction, Request, Response } from 'express'
import * as requestIp from 'request-ip'

export default (request: Request, response: Response, next: NextFunction) => {
  const ip = requestIp.getClientIp(request);
  console.log(`Client Address: ${ip}, ${request.method} ${request.path}`)
  next()
}
