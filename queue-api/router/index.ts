import { Router } from "express";
import { checkAuthToken, trafficLog } from "../middleware";
import nfmRouter from "./nfm";
import sharedRouter from "./shared";
import sicmRouter from "./sicm";

const router: Router = Router();
router.use("/", trafficLog, checkAuthToken, sharedRouter)
if(process.env.CHANNEL_ID==="sicm")
router.use("/sicm", trafficLog, checkAuthToken, sicmRouter)
if(process.env.CHANNEL_ID==="nfm")
router.use("/nfm", trafficLog, checkAuthToken, nfmRouter)


export default router