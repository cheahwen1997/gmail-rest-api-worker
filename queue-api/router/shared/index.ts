import { Router } from 'express';
import mailRouter from '../shared/mail.router';
const router: Router = Router()
router.use('/mail', mailRouter)
export default router;