import type { NextFunction, Request, Response } from 'express'
import { Router } from 'express'
import { QueueTypes } from 'shared/enums'
import QueueService from 'shared/services/queue.service'
import SharedMailService from 'shared/services/shared/mail.service'
import * as utils from 'shared/utils'

const mailRouter: Router = Router()

mailRouter.post(
  '/',
  utils.apiErrorCatchAsync(
    async (req: Request, res: Response, next: NextFunction): Promise<void> => {
      if (!req.headers['x-channel-id'])
        throw new Error(`x-channel-id is not specified`)

      await utils.joi.validations.validateMailPayload(req.body)

      const channel = req.headers['x-channel-id'] as string

      if (process.env.ENABLE_WORKER === "true") {
        const QueueServiceInstance = QueueService.bootstrap()
        QueueServiceInstance.instantiateQueues(
          `${channel}::${process.env.STAGE}`
        )
        const job = await QueueServiceInstance.getQueue(QueueTypes.MAIL).add(
          'mailJob',
          req.body
        )
      } else {
        await SharedMailService.sendMail(req.body)
      }

      res.status(200).json({
        status: 200,
        message: 'ok',
        data: { mail: req.body }
      })
    }
  )
)

export default mailRouter
