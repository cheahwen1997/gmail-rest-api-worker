import { Router } from 'express';
import reportRouter from "./report.router";
const router: Router = Router()
router.use('/report', reportRouter)
export default router;