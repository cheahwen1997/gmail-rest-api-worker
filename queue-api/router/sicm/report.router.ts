import type { NextFunction, Request, Response } from 'express'
import { Router } from 'express'
import { QueueTypes } from 'shared/enums'
import QueueService from 'shared/services/queue.service'
import SICMReportService from 'shared/services/sicm/report.service'
import * as utils from 'shared/utils'

const router: Router = Router()

router.post(
  '/reminder',
  utils.apiErrorCatchAsync(
    async (req: Request, res: Response, next: NextFunction): Promise<void> => {
      if (!req.headers['x-channel-id'])
        throw new Error(`x-channel-id is not specified`)

      await utils.joi.validations.validateProductionReportInputPayload(req.body)

      const channel = req.headers['x-channel-id'] as string

      if (process.env.ENABLE_WORKER === "true") {
        const QueueServiceInstance = QueueService.bootstrap()
        QueueServiceInstance.instantiateQueues(
          `${channel}::${process.env.STAGE}`
        )
        const job = await QueueServiceInstance.getQueue(
          QueueTypes.REMINDER
        ).add('reminderJob', req.body)
      } else {
        await SICMReportService.sendReminderMail(req.body.yearMonth)
      }

      res.status(200).json({
        status: 200,
        message: 'ok',
        data: {}
      })
    }
  )
)

router.post(
  '/summary',
  utils.apiErrorCatchAsync(
    async (req: Request, res: Response, next: NextFunction): Promise<void> => {
      if (!req.headers['x-channel-id'])
        throw new Error(`x-channel-id is not specified`)

      await utils.joi.validations.validateProductionReportInputPayload(req.body)

      const channel = req.headers['x-channel-id'] as string
      const QueueServiceInstance = QueueService.bootstrap()
      QueueServiceInstance.instantiateQueues(`${channel}::${process.env.STAGE}`)
      if (process.env.ENABLE_WORKER === "true") {
        const job = await QueueServiceInstance.getQueue(QueueTypes.SUMMARY).add(
          'summaryJob',
          req.body
        )
      } else {
        await SICMReportService.sendSubmissionReportMail(req.body.yearMonth)
      }
      res.status(200).json({
        status: 200,
        message: 'ok',
        data: {}
      })
    }
  )
)

export default router
