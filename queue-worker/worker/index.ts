import nfmWorker from './nfm'
import sharedWorker from './shared/mail.work'
import sicmWorker from './sicm'

function worker(channelId: string) {
  sharedWorker(channelId)
  if (process.env.CHANNEL_ID === 'sicm') sicmWorker(process.env.CHANNEL_ID)
  if (process.env.CHANNEL_ID === 'nfm') nfmWorker(process.env.CHANNEL_ID)
}

export default worker
