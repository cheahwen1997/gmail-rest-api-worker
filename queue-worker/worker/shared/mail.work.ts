import { Worker } from 'bullmq'
import IORedis from 'ioredis'
import { QueueTypes } from 'shared/enums'
import Gmail from 'shared/services/gmail.service'
import Redis from 'shared/services/redis.service'
import SharedMailService from 'shared/services/shared/mail.service'

const GmailInstance: Gmail = Gmail.bootstrap()
const RedisInstance: IORedis = Redis.bootstrap()

export default function (channel) {
  const mailWorker = new Worker(
    QueueTypes.MAIL,
    async (job) => {
      await SharedMailService.sendMail(job.data)
    },
    {
      prefix: `${channel}::${process.env.STAGE}`,
      connection: RedisInstance
    }
  )

  mailWorker.on('ready', () => {
    console.log(
      `[${channel}] Mail worker ready and listen on queue: ${QueueTypes.MAIL}`
    )
  })

  mailWorker.on('completed', (job) => {
    console.log(`sending email success: ${JSON.stringify(job)}`)
  })

  mailWorker.on('failed', (job, err) => {
    console.error(`${job?.id} has failed with ${err.message}`)
  })
}
