import { Worker } from 'bullmq'
import IORedis from 'ioredis'
import { QueueTypes } from 'shared/enums'
import Redis from 'shared/services/redis.service'
import { IProductionReportInput } from 'shared/types/mailer'
import SICMReportService from 'shared/services/sicm/report.service'

const RedisInstance: IORedis = Redis.bootstrap()

export default function (channel) {
  // gm
  const gSubmitReportReminderWorker = new Worker<IProductionReportInput>(
    QueueTypes.REMINDER,
    async (job) => {
      await SICMReportService.sendReminderMail(job.data.yearMonth)
    },
    {
      prefix: `${channel}::${process.env.STAGE}`,
      connection: RedisInstance
    }
  )

  gSubmitReportReminderWorker.on('ready', () => {
    console.log(
      `[${channel}] Worker ready and listen on queue: ${QueueTypes.REMINDER}`
    )
  })

  gSubmitReportReminderWorker.on('completed', (job) => {
    console.log(`${QueueTypes.REMINDER}::Completed`)
  })

  gSubmitReportReminderWorker.on('failed', (job, err) => {
    console.error(
      `${QueueTypes.REMINDER}::${job?.id} has failed with ${err.message}`
    )
  })

  // send email for admin
  const gmReportSubmissionSummary = new Worker<IProductionReportInput>(
    QueueTypes.SUMMARY,
    async (job) => {
      await SICMReportService.sendSubmissionReportMail(job.data.yearMonth)
    },
    {
      prefix: `${channel}::${process.env.STAGE}`,
      connection: RedisInstance
    }
  )

  gmReportSubmissionSummary.on('ready', () => {
    console.log(
      `[${channel}] Worker ready and listen on queue: ${QueueTypes.SUMMARY}`
    )
  })

  gmReportSubmissionSummary.on('completed', (job) => {
    console.log(`${QueueTypes.SUMMARY}::Completed`)
  })

  gmReportSubmissionSummary.on('failed', (job, err) => {
    console.error(
      `${QueueTypes.SUMMARY}::${job?.id} has failed with ${err.message}`
    )
  })
}
