import { Position } from 'shared/enums/sicm'

export const excelHeaderUserArr = [
  'Month',
  'GMName',
  'GMAccountNumber',
  'IOAccountNumber',
  'AccountNumber',
  'Email',
  'Name',
  'Position',
  'BusinessType',
  'Region'
]
export const excelHeaderProductionArr = [
  'QFYP_PS_M',
  'QFYP_PS_T',
  'QFYP_DU_M',
  'QFYP_DU_T',
  'QFYP_DG_M',
  'QFYP_DG_T',
  'QFYP_WG_M',
  'QFYP_WG_T',
  'ANP_WITHOUT_GSR_PS_M',
  'ANP_WITHOUT_GSR_PS_T',
  'ANP_WITHOUT_GSR_DU_M',
  'ANP_WITHOUT_GSR_DU_T',
  'ANP_WITHOUT_GSR_DG_M',
  'ANP_WITHOUT_GSR_DG_T',
  'ANP_WITHOUT_GSR_WG_M',
  'ANP_WITHOUT_GSR_WG_T',
  'ANP_WITHOUT_GSR_WG_M_FULLCOUNT',
  'ANP_WITHOUT_GSR_WG_T_FULLCOUNT',
  'ANP_WITH_GSR_PS_M',
  'ANP_WITH_GSR_PS_T',
  'ANP_WITH_GSR_DU_M',
  'ANP_WITH_GSR_DU_T',
  'ANP_WITH_GSR_DG_M',
  'ANP_WITH_GSR_DG_T',
  'ANP_WITH_GSR_WG_M',
  'ANP_WITH_GSR_WG_T',
  'ANP_WITH_GSR_WG_M_FULLCOUNT',
  'ANP_WITH_GSR_WG_T_FULLCOUNT',
  'NOC_PS_M',
  'NOC_PS_T',
  'NOC_DU_M',
  'NOC_DU_T',
  'NOC_DG_M',
  'NOC_DG_T',
  'NOC_WG_M',
  'NOC_WG_T',
  'QFYP_WU_M',
  'QFYP_WU_T',
  'ANP_WITHOUT_GSR_WU_M',
  'ANP_WITHOUT_GSR_WU_T',
  'ANP_WITH_GSR_WU_M',
  'ANP_WITH_GSR_WU_T',
  'NOC_WU_M',
  'NOC_WU_T'
]

export const checklistMatrix = [
  {
    positions: [Position.SGM, Position.GM, Position.GAM],
    fieldsCheck: excelHeaderProductionArr.filter((x) => !x.includes('WU'))
  },
  {
    positions: [Position.AM, Position.UM],
    fieldsCheck: excelHeaderProductionArr.filter((x) => !x.includes('WG') && !x.includes('WU'))
  },
  {
    positions: [Position.LIA, Position.CA],
    fieldsCheck: excelHeaderProductionArr.filter((x) => x.includes('PS'))
  },
  {
    positions: [Position.AGT, Position.FTA, Position.RA],
    fieldsCheck: excelHeaderProductionArr.filter(
      (x) =>
        x.includes('PS') &&
        x !== 'ANP_WITHOUT_GSR_PS_M' &&
        x !== 'ANP_WITHOUT_GSR_PS_T' &&
        x !== 'ANP_WITH_GSR_PS_M' &&
        x !== 'ANP_WITH_GSR_PS_T'
    )
  }
]
