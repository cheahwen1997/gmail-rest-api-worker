export enum QueueTypes {
  DEFAULT = 'default',
  MAIL = 'mail',
  REMINDER = 'reminder',
  SUMMARY = 'summary'
}
