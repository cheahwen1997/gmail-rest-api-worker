export enum Position {
  SGM = 'SGM',
  GM = 'GM',
  GAM = 'GAM',
  AM = 'AM',
  UM = 'UM',
  FTA = 'FTA',
  AGT = 'AGT',
  CA = 'CA',
  LIA = 'LIA',
  RA = 'RA'
}