import { RowDataPacket } from 'mysql2'
import dbPool from '../db'
import moment from 'moment'
import { Position } from '../enums/sicm'
export interface IUser extends RowDataPacket {
  AccountNumber?: string
  UserName: string
  GMName: string
  GMAccountNumber: string
  IOAccountNumber: string
  Email: string
  Position: string
  BusinessType: string
  Region: string

  // production field
  QFYP_PS_M: string
  QFYP_PS_T: string
  QFYP_DU_M: string
  QFYP_DU_T: string
  QFYP_DG_M: string
  QFYP_DG_T: string
  QFYP_WG_M: string
  QFYP_WG_T: string
  ANP_WITHOUT_GSR_PS_M: string
  ANP_WITHOUT_GSR_PS_T: string
  ANP_WITHOUT_GSR_DU_M: string
  ANP_WITHOUT_GSR_DU_T: string
  ANP_WITHOUT_GSR_DG_M: string
  ANP_WITHOUT_GSR_DG_T: string
  ANP_WITHOUT_GSR_WG_M: string
  ANP_WITHOUT_GSR_WG_T: string
  ANP_WITHOUT_GSR_WG_M_FULLCOUNT: string
  ANP_WITHOUT_GSR_WG_T_FULLCOUNT: string
  ANP_WITH_GSR_PS_M: string
  ANP_WITH_GSR_PS_T: string
  ANP_WITH_GSR_DU_M: string
  ANP_WITH_GSR_DU_T: string
  ANP_WITH_GSR_DG_M: string
  ANP_WITH_GSR_DG_T: string
  ANP_WITH_GSR_WG_M: string
  ANP_WITH_GSR_WG_T: string
  ANP_WITH_GSR_WG_M_FULLCOUNT: string
  ANP_WITH_GSR_WG_T_FULLCOUNT: string
  NOC_PS_M: string
  NOC_PS_T: string
  NOC_DU_M: string
  NOC_DU_T: string
  NOC_DG_M: string
  NOC_DG_T: string
  NOC_WG_M: string
  NOC_WG_T: string
  QFYP_WU_M: string
  QFYP_WU_T: string
  ANP_WITHOUT_GSR_WU_M: string
  ANP_WITHOUT_GSR_WU_T: string
  ANP_WITH_GSR_WU_M: string
  ANP_WITH_GSR_WU_T: string
  NOC_WU_M: string
  NOC_WU_T: string
}

const ProductionInfoRepo = {
  getAllUsers(yearmonth: string, positions: Position[]): Promise<IUser[]> {
    const validEffectiveDate = moment(yearmonth, 'YYYYMM')
      .endOf('M')
      .format('YYMMDD')
    const sql = `SELECT
      a.*
    FROM
        UserInfo a JOIN UserStatus b ON a.AccountNumber = b.AccountNumber and b.Status = 1 AND b.EffectiveDate < ${validEffectiveDate}
    WHERE
        (
          ${
            positions.length <= 0
              ? `1=1`
              : positions.map((p) => `a.POSITION = '${p}'`).join(' OR ')
          }
        )
        ORDER BY a.GMAccountNumber`

    console.log(`Running query: ${sql}`)
    return new Promise((resolve, reject) => {
      return dbPool.getConnection((err, conn) => {
        if (err) {
          console.log('DB Connect Error')
          console.log(err)
          if (typeof conn !== 'undefined' && conn) {
            conn.release()
            console.log('Release connection due to error')
          }
          return reject(err)
        }
        return conn.query<IUser[]>(sql, (err, res) => {
          if (typeof conn !== 'undefined' && conn) {
            conn.release()
            console.log('Release connection')
          }
          if (err) reject(err)
          else resolve(res)
        })
      })
    })
  },
  getUsersFullSubmitReport(
    yearmonth: string,
    positions: Position[],
    isAssociate: boolean | null | undefined,
    gmAccountNumber: string | null | undefined,
    fieldsCheck: string[]
  ): Promise<IUser[]> {
    const validEffectiveDate = moment(yearmonth, 'YYYYMM')
      .endOf('M')
      .format('YYMMDD')

    const sql = `SELECT 
      a.*,  
      c.*
      FROM 
        UserInfo a 
        JOIN UserStatus b ON a.AccountNumber = b.AccountNumber 
        AND b.Status = 1 AND b.EffectiveDate < ${validEffectiveDate}
        JOIN ProductionInfo c ON a.AccountNumber = c.AccountNumber 
      WHERE 
        (
          ${
            positions.length <= 0
              ? `1=1`
              : positions.map((p) => `a.POSITION = '${p}'`).join(' OR ')
          }
        )
        AND ${
          isAssociate
            ? `a.GMName = 'ASSOCIATE'`
            : '1=1'
        }
        AND ${
          gmAccountNumber
            ? isAssociate
              ? `a.IOAccountNumber = '${gmAccountNumber}'`
              : `a.GMAccountNumber = '${gmAccountNumber}'`
            : '1=1'
        }
        AND c.MonthInfo = ${yearmonth} 
        AND (${
          fieldsCheck.length <= 0
            ? `1=1`
            : fieldsCheck.map((c) => `${c} IS NOT NUll`).join(' AND \n')
        })
        ORDER BY a.GMAccountNumber`

    console.log(`Running query: ${sql}`)

    return new Promise((resolve, reject) => {
      return dbPool.getConnection((err, conn) => {
        if (err) {
          ;``
          console.log('DB Connect Error')
          console.log(err)
          if (typeof conn !== 'undefined' && conn) {
            conn.release()
            console.log('Release connection due to error')
          }
          return reject(err)
        }
        return conn.query<IUser[]>(sql, (err, res) => {
          if (typeof conn !== 'undefined' && conn) {
            conn.release()
            console.log('Release connection')
          }
          if (err) reject(err)
          else resolve(res)
        })
      })
    })
  },
  getUsersPartialSubmitReport(
    yearmonth: string,
    positions: Position[],
    isAssociate: boolean | null | undefined,
    gmAccountNumber: string | null | undefined,
    fieldsCheck: string[]
  ): Promise<IUser[]> {
    const validEffectiveDate = moment(yearmonth, 'YYYYMM')
      .endOf('M')
      .format('YYMMDD')

    const sql = `SELECT 
      a.*,  
      c.*
      FROM 
        UserInfo a 
        JOIN UserStatus b ON a.AccountNumber = b.AccountNumber 
        AND b.Status = 1 AND b.EffectiveDate < ${validEffectiveDate}
        JOIN ProductionInfo c ON a.AccountNumber = c.AccountNumber 
      WHERE 
        (
          ${
            positions.length <= 0
              ? `1=1`
              : positions.map((p) => `a.POSITION = '${p}'`).join(' OR ')
          }
        )
        AND ${
          isAssociate
            ? `a.GMName = 'ASSOCIATE'`
            : '1=1'
        }
        AND ${
          gmAccountNumber
            ? isAssociate
              ? `a.IOAccountNumber = '${gmAccountNumber}'`
              : `a.GMAccountNumber = '${gmAccountNumber}'`
            : '1=1'
        }
        AND c.MonthInfo = ${yearmonth} 
        AND (${
          fieldsCheck.length <= 0
            ? `1=1`
            : fieldsCheck.map((c) => `${c} IS NUll`).join(' OR \n')
        })
        ORDER BY a.GMAccountNumber`

    console.log(`Running query: ${sql}`)

    return new Promise((resolve, reject) => {
      return dbPool.getConnection((err, conn) => {
        if (err) {
          console.log('DB Connect Error')
          console.log(err)
          if (typeof conn !== 'undefined' && conn) {
            conn.release()
            console.log('Release connection due to error')
          }
          return reject(err)
        }
        return conn.query<IUser[]>(sql, (err, res) => {
          if (typeof conn !== 'undefined' && conn) {
            conn.release()
            console.log('Release connection')
          }
          if (err) reject(err)
          else resolve(res)
        })
      })
    })
  },
  getUsersNotSubmitReport(
    yearmonth: string,
    positions: Position[],
    isAssociate: boolean | null | undefined,
    gmAccountNumber: string | null | undefined,
    fieldsCheck: string[]
  ): Promise<IUser[]> {
    const validEffectiveDate = moment(yearmonth, 'YYYYMM')
      .endOf('M')
      .format('YYMMDD')

    const sql = `SELECT
              a.*,
              b.Status,
              ${fieldsCheck.map((f) => `NULL as ${f}`).join(',\n')}
          FROM
              UserInfo a JOIN UserStatus b ON a.AccountNumber = b.AccountNumber and b.Status = 1 AND b.EffectiveDate < ${validEffectiveDate}
          WHERE
              (
                ${
                  positions.length <= 0
                    ? `1=1`
                    : positions.map((p) => `a.POSITION = '${p}'`).join(' OR ')
                }
              )
              AND ${
                isAssociate
                  ? `a.GMName = 'ASSOCIATE'`
                  : '1=1'
              }
              AND ${
                gmAccountNumber
                  ? isAssociate
                    ? `a.IOAccountNumber = '${gmAccountNumber}'`
                    : `a.GMAccountNumber = '${gmAccountNumber}'`
                  : '1=1'
              }
              AND a.AccountNumber NOT IN (
                  SELECT
                      AccountNumber
                  FROM
                      ProductionInfo
                  WHERE
                      MonthInfo = '${yearmonth}'
        )
        ORDER BY a.GMAccountNumber
        `

    console.log(`Running query: ${sql}`)
    return new Promise((resolve, reject) => {
      return dbPool.getConnection((err, conn) => {
        if (err) {
          console.log('DB Connect Error')
          console.log(err)
          if (typeof conn !== 'undefined' && conn) {
            conn.release()
            console.log('Release connection due to error')
          }
          return reject(err)
        }
        return conn.query<IUser[]>(sql, (err, res) => {
          if (typeof conn !== 'undefined' && conn) {
            conn.release()
            console.log('Release connection')
          }
          if (err) reject(err)
          else resolve(res)
        })
      })
    })
  }
}

export default ProductionInfoRepo
