import { Auth, google } from 'googleapis'
import { IMailPayload } from 'shared/types/mailer'
import * as utils from 'shared/utils'
import path from 'path'
import oauthMailToken from 'shared/../.gcp/oauthMailToken.json'

class Gmail {
  client: Auth.JWT | Auth.OAuth2Client

  private static instance: Gmail

  public static bootstrap(isOauth = true): Gmail {
    if (!this.instance) {
      this.instance = new Gmail()
      if (!isOauth)
        this.instance.configureJWT({
          keyFile: path.resolve(process.env.OAUTH_TOKEN_PATH ?? ''),
          scopes: ['https://www.googleapis.com/auth/gmail.send'],
          subject: process.env.MAIL_SENDER_EMAIL
        } as Auth.JWT)
      else
        this.instance.configureOauth({
          _clientId: oauthMailToken._clientId,
          _clientSecret: oauthMailToken._clientSecret,
          credentials: oauthMailToken.credentials
        } as Auth.OAuth2Client)
    }
    return this.instance
  }

  configureJWT(config: Auth.JWT) {
    console.log('Configure gmail service account')

    this.client = new google.auth.JWT({
      keyFile: config.keyFile,
      scopes: config.scopes,
      subject: config.subject
    })
  }
  configureOauth(config: Auth.OAuth2Client) {
    console.log('Configure gmail oauth')

    this.client = new google.auth.OAuth2({
      clientId: config._clientId,
      clientSecret: config._clientSecret
    })
    this.client.setCredentials({
      refresh_token: config?.credentials.refresh_token
    })
  }

  async sendMail(payload: IMailPayload): Promise<any> {
    await utils.joi.validations.validateMailPayload(payload)

    const encodedMessage: string = await utils.mailer.encodeMessage(payload)

    let gmailResponse: any = await google
      .gmail({
        version: 'v1',
        auth: this.client
      })
      .users.messages.send({
        userId: 'me',
        requestBody: {
          raw: encodedMessage
        }
      })

    return gmailResponse
  }
}

export default Gmail
