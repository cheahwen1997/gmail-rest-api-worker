import { Queue, QueueOptions } from 'bullmq';
import { QueueTypes } from 'shared/enums';

export default class QueueService {
    private queues: Record<string, Queue>;

    private static instance: QueueService;

    public static bootstrap(): QueueService {
        if (!this.instance) {
            this.instance = new QueueService();
        }
        return this.instance;
    }

    constructor() {
        if (QueueService.instance instanceof QueueService) {
            return QueueService.instance;
        }
        this.queues = {};
        QueueService.instance = this;
    }

    async instantiateQueues(prefix: string) {
        const QUEUE_OPTIONS: QueueOptions = {
            prefix,
            defaultJobOptions: {
                attempts: 3,
                backoff: {
                    type: 'exponential',
                    delay: 1000,
                  },
                removeOnComplete: false, // this indicates if the job should be removed from the queue once it's complete
                removeOnFail: false, // this indicates if the job should be removed from the queue if it fails
            },
            connection: {
                // redis server connection options
                host: process.env.REDIS_HOST,
                port: Number(process.env.REDIS_PORT ?? 6379),
            },
        };

        this.queues[QueueTypes.DEFAULT] = new Queue(QueueTypes.DEFAULT, QUEUE_OPTIONS);
        this.queues[QueueTypes.MAIL] = new Queue(QueueTypes.MAIL, QUEUE_OPTIONS);
        this.queues[QueueTypes.SUMMARY] = new Queue(QueueTypes.SUMMARY, QUEUE_OPTIONS);
        this.queues[QueueTypes.REMINDER] = new Queue(QueueTypes.REMINDER, QUEUE_OPTIONS);

    }

    getQueue(name: QueueTypes) {
        return this.queues[name];
    }
}