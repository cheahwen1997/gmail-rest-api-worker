import IORedis from 'ioredis'

export default class Redis {
  private static instance: IORedis

  public static bootstrap(): IORedis {
    if (!this.instance && process.env.ENABLE_WORKER === 'true') {
      this.instance = new IORedis({
        // redis server connection options
        host: process.env.REDIS_HOST,
        port: Number(process.env.REDIS_PORT ?? 6379),
        maxRetriesPerRequest: null
      })
    }
    return this.instance
  }
}
