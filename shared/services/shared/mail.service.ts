import Gmail from 'shared/services/gmail.service'
import { IMailPayload } from 'shared/types/mailer'

const GmailInstance: Gmail = Gmail.bootstrap()

const SharedMailService = {
  sendMail: async (payload: IMailPayload) => {
    console.log('Perform Sending email')
    console.log('Job content')
    console.log(payload)

    let gmailResponse: any = await GmailInstance.sendMail({
      ...payload,
      sender: {
        name: process.env.MAIL_SENDER_NAME,
        email: process.env.MAIL_SENDER_EMAIL
      }
    } as IMailPayload)

    console.log('Send email successfully')
    console.log(gmailResponse?.data)
  }
}

export default SharedMailService
