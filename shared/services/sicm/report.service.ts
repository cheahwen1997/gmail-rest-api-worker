import { randomUUID } from 'crypto'
import * as fs from 'fs'
import { compile } from 'handlebars'
import mjml2html from 'mjml'
import moment from 'moment'
import { resolve } from 'path'
import {
  checklistMatrix,
  excelHeaderProductionArr,
  excelHeaderUserArr
} from 'shared/config/sicm'
import { QueueTypes } from 'shared/enums'
import { Position } from 'shared/enums/sicm'
import ProductionInfoRepo from 'shared/repo/ProductionInfoRepo'
import Gmail from 'shared/services/gmail.service'
import XLSXUtils from 'shared/utils/xlsx'

const GmailInstance: Gmail = Gmail.bootstrap()

const excelHeader = [...excelHeaderUserArr, ...excelHeaderProductionArr]

const cellStyleFn = (row, cell) => {
  if (String(cell.value)?.toLowerCase() === 'skip') {
    cell.font = {
      bold: true,
      color: {
        argb: 'FFFFFF'
      }
    }
  }

  if (String(cell.value)?.toLowerCase() === 'skip') {
    cell.fill = {
      type: 'pattern',
      pattern: 'solid',
      fgColor: { argb: 'D3D3D3' }
    }
  }
}

const SICMReportService = {
  sendReminderMail: async (yearMonth: string) => {
    console.log(`${QueueTypes.REMINDER}::Start task`)

    if (!process.env.MAIL_SENDER_NAME || !process.env.MAIL_SENDER_EMAIL) {
      console.log(
        `${QueueTypes.REMINDER}::Undefined mail sender name or mail sender email`
      )
      return
    }

    const system_report_year_month = `${moment(yearMonth, 'YYYYMM')
      .startOf('month')
      .format('MMMM YYYY')}`

    let ProductionReportReminderTemplate = mjml2html(
      fs.readFileSync('shared/mjml/ProductionReportReminder.mjml', 'utf8'),
      {
        beautify: true,
        filePath: resolve('shared/mjml'),
        minify: true
      }
    )

    for (const m of [
      {
        gmAccountNumber: null, //TODO: replace to NULL, this is testing
        isAssociate: false,
        positions: [Position.SGM, Position.GM, Position.GAM],
        fieldsCheck: checklistMatrix[0].fieldsCheck
      },
      {
        gmAccountNumber: '0000000',
        isAssociate: true,
        positions: [Position.AM, Position.UM],
        fieldsCheck: checklistMatrix[1].fieldsCheck
      }
    ]) {
      const notSubmitUsers = await ProductionInfoRepo.getUsersNotSubmitReport(
        yearMonth,
        m.positions,
        m.isAssociate,
        m.gmAccountNumber,
        m.fieldsCheck
      )

      const partialSubmitUsers =
        await ProductionInfoRepo.getUsersPartialSubmitReport(
          yearMonth,
          m.positions,
          m.isAssociate,
          m.gmAccountNumber,
          m.fieldsCheck
        )

      // TODO: remove full submit dont need remind them
      // const fullSubmitUsers = await ProductionInfoRepo.getUsersFullSubmitReport(
      //   yearMonth,
      //   m.positions,
      //   m.isAssociate,
      //   m.gmAccountNumber,
      //   m.fieldsCheck
      // )

      for (const receipentInfo of [
        ...partialSubmitUsers,
        ...notSubmitUsers,
        // ...fullSubmitUsers
      ]) {
        if (!receipentInfo.Email) continue

        const folder = `./tmp/sicm/${
          process.env.STAGE
        }/reminder-${randomUUID()}/`
        if (!fs.existsSync(folder)) {
          fs.mkdirSync(folder, { recursive: true })
        }

        const fileName =
          folder +
          `(${system_report_year_month}) ${receipentInfo.AccountNumber} Upload Status.xlsx`

        let rows: any = []
        if (m.isAssociate) {
          rows = [
            {
              Month: yearMonth,
              AccountNumber: receipentInfo.AccountNumber,
              GMName: receipentInfo.GMName,
              GMAccountNumber: receipentInfo.GMAccountNumber,
              IOAccountNumber: receipentInfo.IOAccountNumber,
              Email: receipentInfo.Email,
              Name: receipentInfo.UserName,
              Position: receipentInfo.Position,
              BusinessType: receipentInfo.BusinessType,
              Region: receipentInfo.Region,
              ...excelHeaderProductionArr.reduce(
                (a, b) => ({
                  ...a,
                  [b]: m.fieldsCheck.find((fc) => fc === b)
                    ? receipentInfo[b] != null && String(receipentInfo[b]) != ''
                      ? '✓'
                      : '✕'
                    : 'skip'
                }),
                {}
              )
            }
          ]
        }

        // find those user under this gm account number
        for (const matrix of checklistMatrix) {
          const submitUsers = await ProductionInfoRepo.getUsersFullSubmitReport(
            yearMonth,
            matrix.positions,
            m.isAssociate,
            receipentInfo.AccountNumber, // this will be gm account number / associate
            matrix.fieldsCheck
          )

          const csvDataSubmit = submitUsers.map((info) => {
            return {
              Month: yearMonth,
              GMName: info.GMName,
              GMAccountNumber: info.GMAccountNumber,
              IOAccountNumber: info.IOAccountNumber,
              AccountNumber: info.AccountNumber,
              Email: info.Email,
              Name: info.UserName,
              Position: info.Position,
              BusinessType: info.BusinessType,
              Region: info.Region,
              ...excelHeaderProductionArr.reduce(
                (a, b) => ({
                  ...a,
                  [b]: matrix.fieldsCheck.find((fc) => fc === b)
                    ? info[b] != null && String(info[b]) != ''
                      ? '✓'
                      : '✕'
                    : 'skip'
                }),
                {}
              )
            }
          })

          rows = [...rows, ...csvDataSubmit]

          const partialSubmitUsers =
            await ProductionInfoRepo.getUsersPartialSubmitReport(
              yearMonth,
              matrix.positions,
              m.isAssociate,
              receipentInfo.AccountNumber, // this will be gm account number / associate
              matrix.fieldsCheck
            )

          const csvDataPartialSubmit = partialSubmitUsers.map((info) => {
            return {
              Month: yearMonth,
              GMName: info.GMName,
              GMAccountNumber: info.GMAccountNumber,
              IOAccountNumber: info.IOAccountNumber,
              AccountNumber: info.AccountNumber,
              Email: info.Email,
              Name: info.UserName,
              Position: info.Position,
              BusinessType: info.BusinessType,
              Region: info.Region,
              ...excelHeaderProductionArr.reduce(
                (a, b) => ({
                  ...a,
                  [b]: matrix.fieldsCheck.find((fc) => fc === b)
                    ? info[b] != null && String(info[b]) != ''
                      ? '✓'
                      : '✕'
                    : 'skip'
                }),
                {}
              )
            }
          })

          rows = [...rows, ...csvDataPartialSubmit]

          const notSubmitUsers =
            await ProductionInfoRepo.getUsersNotSubmitReport(
              yearMonth,
              matrix.positions,
              m.isAssociate,
              receipentInfo.AccountNumber, // this will be gm account number / associate
              matrix.fieldsCheck
            )

          const csvDataNotSubmit = notSubmitUsers.map((info) => {
            return {
              Month: yearMonth,
              GMName: info.GMName,
              GMAccountNumber: info.GMAccountNumber,
              IOAccountNumber: info.IOAccountNumber,
              AccountNumber: info.AccountNumber,
              Email: info.Email,
              Name: info.UserName,
              Position: info.Position,
              BusinessType: info.BusinessType,
              Region: info.Region,
              ...excelHeaderProductionArr.reduce(
                (a, b) => ({
                  ...a,
                  [b]: matrix.fieldsCheck.find((fc) => fc === b)
                    ? info[b] != null && String(info[b]) != ''
                      ? '✓'
                      : '✕'
                    : 'skip'
                }),
                {}
              )
            }
          })

          rows = [...rows, ...csvDataNotSubmit]
        }

        await XLSXUtils.export([
          {
            fileName,
            sheets: [
              {
                sheetName: `Upload Status`,
                rows
              }
            ].map(({ sheetName, rows }) => {
              return {
                name: sheetName,
                options: {
                  properties: {
                    tabColor: {
                      argb: ((Math.random() * 0xffffff) << 0)
                        .toString(16)
                        .padStart(6, '0')
                    }
                  }
                },
                columns: excelHeader.map((h) => ({ header: h, key: h })),
                rows,
                cellStyleFn
              }
            })
          }
        ])

        let gmailResponse: any = await GmailInstance.sendMail({
          subject: `${process.env.MAIL_SUBJECT_PREFIX} Reminder For Production Report Submission ${system_report_year_month}`,
          html: compile(ProductionReportReminderTemplate?.html)({
            system_email_name: receipentInfo.UserName,
            system_report_year_month,
            system_year: new Date().getFullYear()
          }),
          receipents: [
            {
              name: receipentInfo.UserName,
              email:
                process.env.STAGE === 'production' &&
                process.env.DRY_RUN !== 'enabled'
                  ? receipentInfo.Email
                  : 'cheahwen1997@gmail.com'
            }
          ],
          cc:
            process.env.STAGE === 'production' &&
            process.env.DRY_RUN !== 'enabled'
              ? []
              : process.env.STAGE === 'local'
              ? []
              : [
                  {
                    name: process.env.MAIL_SENDER_NAME,
                    email: process.env.MAIL_SENDER_EMAIL
                  },
                  {
                    name: 'Eng Kian',
                    email: 'ektan118@gmail.com'
                  },
                  {
                    name: 'Chee Hong',
                    email: 'chkok2003@yahoo.com'
                  },
                  {
                    name: 'Ms Lee',
                    email: 'sicm13311@gmail.com'
                  }
                ],
          sender: {
            name: process.env.MAIL_SENDER_NAME,
            email: process.env.MAIL_SENDER_EMAIL
          },
          attachments: [fileName]
        })

        console.log('Send email successfully')
        console.log(gmailResponse?.data)

        await fs.promises.unlink(fileName)

        // if it is dev && enabled will break
        // if it is production and enabled will not break
        if (
          process.env.STAGE !== 'production' ||
          process.env.DRY_RUN === 'enabled'
        )
          break
      }
    }
  },
  sendSubmissionReportMail: async (yearMonth: string) => {
    console.log(`${QueueTypes.SUMMARY}::Start task`)

    if (!process.env.MAIL_SENDER_NAME || !process.env.MAIL_SENDER_EMAIL) {
      console.log(
        `${QueueTypes.SUMMARY}::Undefined mail sender name or mail sender email`
      )
      return
    }

    const system_report_year_month = `${moment(yearMonth, 'YYYYMM')
      .startOf('month')
      .format('MMMM YYYY')}`

    let globalCsvDataSubmit: any[] = []
    let globalCsvDataPartialSubmit: any[] = []
    let globalCsvDataNotSubmit: any[] = []

    for (const m of [
      {
        gmAccountNumber: null,
        isAssociate: false,
        positions: [Position.SGM, Position.GM, Position.GAM],
        fieldsCheck: checklistMatrix[0].fieldsCheck
      },
      {
        gmAccountNumber: null,
        isAssociate: true,
        positions: [Position.UM],
        fieldsCheck: checklistMatrix[1].fieldsCheck
      }
    ]) {
      const submitUsers = await ProductionInfoRepo.getUsersFullSubmitReport(
        yearMonth,
        m.positions,
        m.isAssociate,
        m.gmAccountNumber,
        m.fieldsCheck
      )

      const notSubmitUsers = await ProductionInfoRepo.getUsersNotSubmitReport(
        yearMonth,
        m.positions,
        m.isAssociate,
        m.gmAccountNumber,
        m.fieldsCheck
      )

      const partialSubmitUsers =
        await ProductionInfoRepo.getUsersPartialSubmitReport(
          yearMonth,
          m.positions,
          m.isAssociate,
          m.gmAccountNumber,
          m.fieldsCheck
        )

      const csvDataSubmit = submitUsers.map((info) => {
        return {
          Month: yearMonth,
          GMName: info.GMName,
          GMAccountNumber: info.GMAccountNumber,
          IOAccountNumber: info.IOAccountNumber,
          AccountNumber: info.AccountNumber,
          Email: info.Email,
          Name: info.UserName,
          Position: info.Position,
          BusinessType: info.BusinessType,
          Region: info.Region,
          ...excelHeaderProductionArr.reduce(
            (a, b) => ({
              ...a,
              [b]: m.fieldsCheck.find((fc) => fc === b)
                ? info[b] != null && String(info[b]) != ''
                  ? '✓'
                  : '✕'
                : 'skip'
            }),
            {}
          )
        }
      })

      globalCsvDataSubmit = [...globalCsvDataSubmit, ...csvDataSubmit]

      const csvDataPartialSubmit = partialSubmitUsers.map((info) => {
        return {
          Month: yearMonth,
          GMName: info.GMName,
          GMAccountNumber: info.GMAccountNumber,
          IOAccountNumber: info.IOAccountNumber,
          AccountNumber: info.AccountNumber,
          Email: info.Email,
          Name: info.UserName,
          Position: info.Position,
          BusinessType: info.BusinessType,
          Region: info.Region,
          ...excelHeaderProductionArr.reduce(
            (a, b) => ({
              ...a,
              [b]: m.fieldsCheck.find((fc) => fc === b)
                ? info[b] != null && String(info[b]) != ''
                  ? '✓'
                  : '✕'
                : 'skip'
            }),
            {}
          )
        }
      })

      globalCsvDataPartialSubmit = [
        ...globalCsvDataPartialSubmit,
        ...csvDataPartialSubmit
      ]

      const csvDataNotSubmit = notSubmitUsers.map((info) => {
        return {
          Month: yearMonth,
          GMName: info.GMName,
          GMAccountNumber: info.GMAccountNumber,
          IOAccountNumber: info.IOAccountNumber,
          AccountNumber: info.AccountNumber,
          Email: info.Email,
          Name: info.UserName,
          Position: info.Position,
          BusinessType: info.BusinessType,
          Region: info.Region,
          ...excelHeaderProductionArr.reduce(
            (a, b) => ({
              ...a,
              [b]: m.fieldsCheck.find((fc) => fc === b)
                ? info[b] != null && String(info[b]) != ''
                  ? '✓'
                  : '✕'
                : 'skip'
            }),
            {}
          )
        }
      })

      globalCsvDataNotSubmit = [...globalCsvDataNotSubmit, ...csvDataNotSubmit]
    }

    // sort by GMName
    globalCsvDataSubmit = globalCsvDataSubmit.sort((a, b) =>
      a.GMName.localeCompare(b.GMName)
    )

    globalCsvDataPartialSubmit = globalCsvDataPartialSubmit.sort((a, b) =>
      a.GMName.localeCompare(b.GMName)
    )

    globalCsvDataNotSubmit = globalCsvDataNotSubmit.sort((a, b) =>
      a.GMName.localeCompare(b.GMName)
    )

    const folder = `./tmp/sicm/${process.env.STAGE}/summary-${randomUUID()}/`
    if (!fs.existsSync(folder)) {
      fs.mkdirSync(folder, { recursive: true })
    }

    const fileName =
      folder +
      `(${system_report_year_month}) Production Report Submission Summary.xlsx`

    const submitSheetName = `Full Submit Users`

    const partialSubmitSheetName = `Partial Submit Users`

    const notSubmitSheetName = `Not Submit Users`

    await XLSXUtils.export([
      {
        fileName,
        sheets: [
          {
            sheetName: submitSheetName,
            rows: globalCsvDataSubmit
          },
          {
            sheetName: partialSubmitSheetName,
            rows: globalCsvDataPartialSubmit
          },
          {
            sheetName: notSubmitSheetName,
            rows: globalCsvDataNotSubmit
          }
        ].map(({ sheetName, rows }) => {
          return {
            name: sheetName,
            options: {
              properties: {
                tabColor: {
                  argb: ((Math.random() * 0xffffff) << 0)
                    .toString(16)
                    .padStart(6, '0')
                }
              }
            },
            columns: excelHeader
              .filter((x) => x!== 'GMName' && x !== 'GMAccountNumber' && x !== 'IOAccountNumber')
              .map((h) => ({ header: h, key: h })),
            rows,
            cellStyleFn
          }
        })
      }
    ])

    let ProductionReportSummaryTemplate = mjml2html(
      fs.readFileSync('shared/mjml/ProductionReportSummary.mjml', 'utf8'),
      {
        beautify: true,
        filePath: resolve('shared/mjml'),
        minify: true
      }
    )

    let gmailResponse: any = await GmailInstance.sendMail({
      subject: `${process.env.MAIL_SUBJECT_PREFIX} Production Report Submission Summary ${system_report_year_month}`,
      html: compile(ProductionReportSummaryTemplate?.html)({
        system_email_name: process.env.MAIL_SENDER_NAME,
        system_report_year_month,
        system_year: new Date().getFullYear()
      }),
      receipents:
        process.env.STAGE === 'local'
          ? [
              {
                name: 'Cheah Wen',
                email: 'cheahwen1997@gmail.com'
              }
            ]
          : [
              {
                name: 'Ms Lee',
                email: 'sicm13311@gmail.com '
              }
            ],
      sender: {
        name: process.env.MAIL_SENDER_NAME,
        email: process.env.MAIL_SENDER_EMAIL
      },
      attachments: [fileName]
    })

    console.log('Send email successfully')
    console.log(gmailResponse?.data)

    // clear tmp excel file
    await fs.promises.unlink(fileName)
  }
}

export default SICMReportService
