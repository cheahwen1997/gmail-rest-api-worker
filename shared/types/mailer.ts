export interface IMailPerson {
  name: string
  email: string
}

export interface IProductionReportInput {
  yearMonth: string
}

export interface IMailPayload {
  sender?: IMailPerson
  receipents: IMailPerson[]
  cc?: IMailPerson[]
  subject: string
  html: string
  attachments?: string[]
}
