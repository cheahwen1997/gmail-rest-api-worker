import type { Request, Response, NextFunction } from "express";

export default (handle: any) => (req: Request, res: Response, next: NextFunction) => {
  
  handle(req, res, next).catch(next)

}

