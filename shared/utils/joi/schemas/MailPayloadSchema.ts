import Joi from 'joi'

export default Joi.object({
  sender: Joi.object().keys({
    name: Joi.string().min(1).required(),
    email: Joi.string().email({ tlds: { allow: false } })
  }),
  receipents: Joi.array()
    .items(
      Joi.object().keys({
        name: Joi.string().required(),
        email: Joi.string()
          .email({ tlds: { allow: false } })
          .required()
      })
    )
    .required()
    .min(1),
  cc: Joi.array().items(
    Joi.object().keys({
      name: Joi.string().required(),
      email: Joi.string()
        .email({ tlds: { allow: false } })
        .required()
    })
  ),
  subject: Joi.string().min(1).required(),
  html: Joi.string().min(1).required(),
  attachments: Joi.array().items(Joi.string())
})
