import Joi from 'joi'

export default Joi.object({
  yearMonth: Joi.string().required().min(6)
})
