import { IProductionReportInput } from 'shared/types/mailer'
import { ProductionReportInputSchema } from '../schemas'

export default function validatePayload(payload: IProductionReportInput): void {
  const { error } = ProductionReportInputSchema.validate(payload)
  if (error)
    throw new Error(
      `Validation error: ${error?.details?.map((e) => e.message).join(',\n')}`
    )
}
