import * as fs from "fs";
import { IMailPayload } from "shared/types/mailer";

export default async function encodeMessage(payload: IMailPayload): Promise<string> {
    let {
        sender,
        receipents,
        cc,
        subject,
        html,
        attachments
    } = payload;

    // You can use UTF-8 encoding for the subject using the method below.
    // You can also just use a plain string if you don't need anything fancy.
    const utf8Subject: string = `=?utf-8?B?${Buffer.from(subject).toString(
        "base64"
    )}?=`;

    let messageParts: string[] = [
        `From: ${sender?.name} <${sender?.email}>`,
        `To: ${receipents
            .map((receipent) => `${receipent.name} <${receipent.email}>`)
            .join(",")}`,
        "MIME-Version: 1.0",
        `Subject: ${utf8Subject}`,
        "Content-Type: multipart/mixed; boundary=012boundary01",
        "",
        "--012boundary01",
        "Content-Type: multipart/alternative; boundary=012boundary02",
        "",
        "--012boundary02",
        "Content-type: text/html; charset=UTF-8",
        "",
        html,
        "",
        "--012boundary02--",
    ];

    if (cc && cc.length > 0)
        messageParts.unshift(
            `cc: ${cc?.map((c) => `${c.name} <${c.email}>`).join(",")}`
        );


    //if any attachments
    if (
        attachments &&
        Array.isArray(attachments) &&
        attachments.length > 0
    ) {
        for (const attachment of attachments) {
            let basename: string = require("path").basename(attachment);

            let attach: string = Buffer.from(
                await fs.promises.readFile(attachment)
            ).toString("base64");

            messageParts = [
                ...messageParts,
                "--012boundary01",
                `Content-Type: Application/octet-stream; name=${basename}`,
                `Content-Disposition: attachment_path; filename=${basename}`,
                "Content-Transfer-Encoding: base64",
                "",
                attach,
                "--012boundary01",
                "",
            ];
        }
        messageParts.push("--012boundary01--");
    }
    const message: string = messageParts.join("\n");

    // The body needs to be base64url encoded.
    const encodedMessage: string = Buffer.from(message)
        .toString("base64")
        .replace(/\+/g, "-")
        .replace(/\//g, "_")
        .replace(/=+$/, "");
    return encodedMessage;
}