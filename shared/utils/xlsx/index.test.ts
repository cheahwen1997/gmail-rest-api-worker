import XLSXUtils from ".";

XLSXUtils.export([
    {
        fileName: "test.xlsx",
        sheets: [
            {
                name: "Test Sheet",
                options: { properties: { tabColor: { argb: 'FF00FF00' } } },
                columns: [
                    {
                        header: "Hi", key: "hi"
                    }
                ],
                rows: [
                    {
                        hi: 1,
                    }
                ],
                cellStyleFn: (row, cell) => {
                    if (cell.value === 'Yes') {
                        cell.fill = {
                            type: 'pattern',
                            pattern: 'solid',
                            fgColor: { argb: 'FF0000' },
                        };

                        cell.font = {
                            bold: true,
                            color: {
                                argb: 'FFFFFF'
                            }
                        }
                    }
                }
            }
        ]
    }
])