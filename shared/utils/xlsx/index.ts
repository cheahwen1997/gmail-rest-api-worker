import * as ExcelJS from "exceljs";

export interface XLSXExportInput {
    fileName: string;
    sheets: {
        name: string;
        options?: Partial<ExcelJS.AddWorksheetOptions> | undefined;
        columns: Partial<ExcelJS.Column>[];
        rows: any;
        cellStyleFn?: (row: ExcelJS.Row, cell: ExcelJS.Cell) => void;
    }[]
}

const XLSXUtils = {
    export: async (exportItems: XLSXExportInput[]) => {
        for (const item of exportItems) {
            const workbook = new ExcelJS.Workbook();
            for (const sheet of item.sheets) {
                const worksheet = workbook.addWorksheet(sheet.name, sheet.options);
                worksheet.columns = sheet.columns;
                let rows = worksheet.addRows(sheet.rows);
                rows.map(row => {

                    row.eachCell((cell) => {

                        if (sheet.cellStyleFn)
                            sheet.cellStyleFn(row, cell);


                    })
                })

                worksheet.columns.forEach(function (column, i) {
                    let maxLength = 0;

                    if (column["eachCell"])
                        column["eachCell"]({ includeEmpty: true }, function (cell) {
                            var columnLength = cell.value ? cell.value.toString().length : 10;
                            if (columnLength > maxLength) {
                                maxLength = columnLength;
                            }
                        });
                    column.width = maxLength < 10 ? 10 : maxLength;
                });
            }
            await workbook.xlsx.writeFile(item.fileName);
        }

    }
}



export default XLSXUtils;