import startWorker from 'queue-worker/worker'

if (process.env.ENABLE_WORKER === 'true') {
  const app = process.env.CHANNEL_ID
  console.log(`Env: ${process.env.STAGE}`)
  console.log(`Running worker: ${app}`)
  if (app) startWorker(app)
} else {
  console.log('Worker disabled. No Job')
}
