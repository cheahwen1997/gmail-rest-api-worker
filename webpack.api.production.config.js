const path = require('path')

module.exports = {
  entry: './startApi.ts',
  mode: 'development',
  target: 'node',
  module: {
    rules: [
      {
        test: /\.ts$/,
        include: [
          path.resolve(__dirname, 'shared'),
          path.resolve(__dirname, 'queue-api')
        ],
        exclude: [path.resolve(__dirname, 'node_modules')],
        use: 'ts-loader'
      }
    ]
  },
  resolve: {
    extensions: ['.ts', '.js'],
    alias: {
      shared: path.resolve(__dirname, 'shared'),
      'queue-api': path.resolve(__dirname, 'queue-api')
    },
  },
  output: {
    publicPath: 'dist',
    filename: 'api.production.js',
    path: path.resolve(__dirname, 'dist'),
    globalObject: `typeof self !== 'undefined' ? self : this`
  }
}
