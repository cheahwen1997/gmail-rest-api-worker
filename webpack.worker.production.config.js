const path = require('path')

module.exports = {
  entry: './startWorker.ts',
  mode: 'development',
  target: 'node',
  module: {
    rules: [
      {
        test: /\.ts$/,
        include: [
          path.resolve(__dirname, 'shared'),
          path.resolve(__dirname, 'queue-worker'),
          path.resolve(__dirname, '.gcp')
        ],
        use: "ts-loader",
      },
    ]
  },
  resolve: {
    extensions: ['.ts', '.js'],
    alias: {
      shared: path.resolve(__dirname, 'shared'),
      'queue-worker': path.resolve(__dirname, 'queue-worker'),
      '.gcp': path.resolve(__dirname, '.gcp')
    },
  },
  output: {
    publicPath: 'dist',
    filename: 'worker.production.js',
    path: path.resolve(__dirname, 'dist'),
    globalObject: `typeof self !== 'undefined' ? self : this`
  }
}
